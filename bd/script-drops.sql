/*Drop las foreign keys primero*/
ALTER TABLE Apartamento DROP FOREIGN KEY fk_Apartment_Building;
ALTER TABLE Apartamento DROP FOREIGN KEY fk_Apartment_Phone;

ALTER TABLE Aprobacion DROP FOREIGN KEY fk_Approval_Repair;
ALTER TABLE Aprobacion DROP FOREIGN KEY fk_Approval_JCOND;

ALTER TABLE Area DROP FOREIGN KEY fk_Zone_Building;
ALTER TABLE Area DROP FOREIGN KEY fk_Zone_Worker;

ALTER TABLE Area_Eval DROP FOREIGN KEY fk_Zone_Eval_1;
ALTER TABLE Area_Eval DROP FOREIGN KEY fk_Zone_Eval_2;

ALTER TABLE Asamblea DROP FOREIGN KEY fk_Assembly_JCOND;

ALTER TABLE Asistencia DROP FOREIGN KEY fk_Assistance_Assembly;
ALTER TABLE Asistencia DROP FOREIGN KEY fk_Assistance_User;

ALTER TABLE Aviso_de_Pago DROP FOREIGN KEY fk_Payment_Warning_Apartment;

ALTER TABLE Buzon DROP FOREIGN KEY fk_Mailbox_User;

ALTER TABLE Cheque DROP FOREIGN KEY fk_Check_JCOND;

ALTER TABLE Condominio DROP FOREIGN KEY fk_Condominium_Apartment;
ALTER TABLE Condominio DROP FOREIGN KEY fk_Condominium_Bill;

ALTER TABLE Contrato DROP FOREIGN KEY fk_Contract_User;
ALTER TABLE Contrato DROP FOREIGN KEY fk_Contratc_Apartment;

ALTER TABLE Cuenta_Bancaria DROP FOREIGN KEY fk_Bank_Account_JCOND;

ALTER TABLE Documento DROP FOREIGN KEY fk_Document_JCOND;

ALTER TABLE Edificio DROP FOREIGN KEY fk_Building_Place;
ALTER TABLE Edificio DROP FOREIGN KEY fk_Building_JCOND;

ALTER TABLE Evaluacion DROP FOREIGN KEY fk_Evaluation_Worker;

ALTER TABLE Factura DROP FOREIGN KEY fk_Bill_User;
ALTER TABLE Factura DROP FOREIGN KEY fk_Bill_Service;

ALTER TABLE Falta DROP FOREIGN KEY fk_Fault_Apartment;

ALTER TABLE Fondo DROP FOREIGN KEY fk_Trustfund_JCOND;

ALTER TABLE Lugar DROP FOREIGN KEY fk_Place_Place;

ALTER TABLE Mora DROP FOREIGN KEY fk_Delay_Fault;

ALTER TABLE Notificacion DROP FOREIGN KEY fk_Notification_Assembly2;
ALTER TABLE Notificacion DROP FOREIGN KEY fk_Notification_JCOND;
ALTER TABLE Notificacion DROP FOREIGN KEY fk_Notification_Requirement;

ALTER TABLE Oficina DROP FOREIGN KEY fk_Office_Place;
ALTER TABLE Oficina DROP FOREIGN KEY fk_Office_JCOND;

ALTER TABLE Presupuesto DROP FOREIGN KEY fk_Budget_Repair;

ALTER TABLE Recargo DROP FOREIGN KEY fk_Recharge_Repair;

ALTER TABLE Reparacion DROP FOREIGN KEY fk_Repair_Zone;

ALTER TABLE Requerimiento DROP FOREIGN KEY fk_Requirement_JCOND;

ALTER TABLE Reu_Usua DROP FOREIGN KEY fk_Reunion_User_1;
ALTER TABLE Reu_Usua DROP FOREIGN KEY fk_Reunion_User_2;

ALTER TABLE Reunion DROP FOREIGN KEY fk_Reunion_JCOND;

ALTER TABLE Serv_Apar DROP FOREIGN KEY fk_Service_Apartment_1;
ALTER TABLE Serv_Apar DROP FOREIGN KEY fk_Service_Apartment_2;

ALTER TABLE Telefono DROP FOREIGN KEY fk_Phone_User;

ALTER TABLE Usua_Apar DROP FOREIGN KEY fk_User_Apartment_1;
ALTER TABLE Usua_Apar DROP FOREIGN KEY fk_User_Apartment_2;

ALTER TABLE Usua_Auto DROP FOREIGN KEY fk_User_Authorization_1;
ALTER TABLE Usua_Auto DROP FOREIGN KEY fk_User_Authorization_2;

ALTER TABLE Usua_Noti DROP FOREIGN KEY fk_User_Notification_1;
ALTER TABLE Usua_Noti DROP FOREIGN KEY fk_User_Notification_2;

ALTER TABLE Usuario DROP FOREIGN KEY fk_User_JCOND;

/*Drop tablas*/

DROP TABLE Apartamento;
DROP TABLE Aprobacion;
DROP TABLE Area;
DROP TABLE Area_Eval;
DROP TABLE Asamblea;
DROP TABLE Asistencia;
DROP TABLE Autorizacion;
DROP TABLE Aviso_de_Pago;
DROP TABLE Buzon;
DROP TABLE Cheque;
DROP TABLE Condominio;
DROP TABLE Contrato;
DROP TABLE Cuenta_Bancaria;
DROP TABLE Documento;
DROP TABLE Edificio;
DROP TABLE Evaluacion;
DROP TABLE Factura;
DROP TABLE Falta;
DROP TABLE Fondo;
DROP TABLE Junta_Condominio;
DROP TABLE Lugar;
DROP TABLE Mora;
DROP TABLE Notificacion;
DROP TABLE Oficina;
DROP TABLE Presupuesto;
DROP TABLE Recargo;
DROP TABLE Reparacion;
DROP TABLE Requerimiento;
DROP TABLE Reu_Usua;
DROP TABLE Reunion;
DROP TABLE Serv_Apar;
DROP TABLE Servicio;
DROP TABLE Telefono;
DROP TABLE Trabajador;
DROP TABLE Usua_Apar;
DROP TABLE Usua_Auto;
DROP TABLE Usua_Noti;
DROP TABLE Usuario;