﻿<?php 
@session_start(); 
 include_once("php/databasehandler.php");
 $dbHandler = new DatabaseHandler();
?>
<!DOCTYPE html>
<html lang="es-ES">
<head>
	<title>Condominios UCAB</title>	
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>	
    
    <meta name="description" content="Condominios UCAB"/>
    <meta name="keywords" content="condominios, ucab, universidad, catolica, andres, bello"/>
    

    <!-- CSS -->    
    <link type="text/css" rel="stylesheet" href="./css/bootstrap.css" media="screen"/>
    <link type="text/css" rel="stylesheet" href="./css/style.css" media="screen"/>
    <link type="text/css" rel="stylesheet" href="./css/j-forms.css" media="screen">
    <!-- JavaScripts -->
         
    <script type="text/javascript" src="js/main.js"></script>  
    <script type="text/javascript" src="js/jquery.1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery.form.min.js"></script>
    <script type="text/javascript" src="js/j-forms.min.js"></script>    
    <script type="text/javascript" src="js/bootstrap.js"></script> 
</head>
<body>

    <?php
        /* componentes de la pagina */
        
        include("php/navbar.php");
        

    ?>

    
    <?php if (isset($_GET['error'])): ?>
        <div class="error-notif">
            <?php if ($_GET['error'] == "badlogin"): ?>
                Número de cédula o contraseña incorrecto
            <?php elseif ($_GET['error'] == "ci_repetida"): ?>
                Esta cédula ya está registrada
            <?php endif; ?>
        </div>
    <?php elseif (isset($_GET['success'])): ?>
        <div class="success">
            <?php if ($_GET['success'] == "buzon_anadido"): ?>
                Se envió  satisfactoriamente tu sugerencia
            <?php elseif($_GET['success'] == "asamblea_creada"): ?>
                Se ha convocado a la asamblea con éxito. Los inquilinos recibirán una notificación automáticamente
            <?php endif; ?>
        </div>
    <?php endif; ?>

    <?php

        if (isset($_SESSION['username']))
        {
            if ($_SESSION['tipo'] == "admin")
                include("php/admin.php");
            else 
                include("php/usuario.php");
        }
    ?>

</body>
</html>


