<?php
	@session_start();
	include_once("php/databasehandler.php");
	$dbHandler = new DatabaseHandler();

	if ($_GET['action'] == "crear_usuario")
	{
		$data = array(
			"nombre" => $_POST['name'],
			"segundo-nombre" => (strlen($_POST['name2']) > 0) ? $_POST['name2'] : null,
			"apellido" => $_POST['lname'],
			"segundo-apellido" => (strlen($_POST['name4']) > 0) ? $_POST['name4'] : null,
			"email" => (strlen($_POST['email']) > 0) ? $_POST['email'] : null,
			"cedula" => $_POST['login'],
			"clave" => $_POST['password'],
			"telefono" => (strlen($_POST['telefono']) > 0) ? $_POST['telefono'] : null,
			"prefijo" => $_POST['prefijo']
		);

		if ($dbHandler->crearUsuario($data))
			header("Location: ./");
		else
			header("Location: ./?error=ci_repetida");
	}
	elseif ($_GET['action'] == "crear_buzon")
	{
		$data = array(
			"tema" => $_POST['reason'],
			"msj" => $_POST['message'],
			"uid" => $_SESSION['username']
		);

		$dbHandler->anadirAlBuzon($data);
		header("Location: ./?success=buzon_anadido");
	}
	elseif ($_GET['action'] == "crear_asamblea")
	{
		$data = array(
			"tipo" => $_POST['reason'],
			"fecha" => $_POST['Mday'],
			"uid" => $_SESSION['username']
		);

		$dbHandler->crearAsamblea($data);
		header("Location: ./?success=asamblea_creada");
	}
	elseif ($_GET['action'] == "eliminar_buzon")
	{
		if ($dbHandler->eliminarBuzon($_POST['bid']))
			echo "true";
		else
			echo "false";

		die();
	}
	elseif ($_GET['action'] == "aprobar_buzon")
	{
		$dbHandler->aprobarBuzon($_POST['bid'], $_POST['area']);
		echo "true";
		die();
	}
	elseif ($_GET['action'] == "anadir_presupuesto")
	{
		$data = array(
			"total" => $_POST['total'],
			"tiempo" => $_POST['tiempoEntrega'],
			"detalles" => $_POST['detalle'],
			"rid" => $_POST['repid']
		);

		$dbHandler->anadirPresupuesto($data);
		die();
	}
	elseif ($_GET['action'] == "aceptar_presupuesto")
	{
		$data = array(
			"rid" => $_POST['rid'],
			"pid" => $_POST['pid']
		);

		$dbHandler->aceptarPresupuesto($data);
		die();
	}
	elseif ($_GET['action'] == "anadir_fondo")
	{
		$dbHandler->anadirFondo($_POST['monto'], $_SESSION['username']);
		header("Location: ./");
		die();
	}
?>