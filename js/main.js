function aprobar(id)
{
	$.ajax({
		url: "process.php?action=aprobar_buzon",
		type: "POST",
		data: {bid:id, area: $("#area"+id+" option:selected").val()},
		success: function(data){
			$("#notif" + id).html("Se ha generado una nueva reparación y se ha enviado una notificación a los inquilinos.");
			$("#notif" + id).css("background-color", "rgb(98, 110, 8)");			
		}
	});
}

function denegar(id)
{
	$.ajax({
		url: "process.php?action=eliminar_buzon",
		type: "POST",
		data: {bid:id},
		success: function(data){
			if (data == "true")
				$("#notif" + id).fadeOut('slow');		
		}
	});
}

function generarPresupuesto(rid)
{
	var precio = Math.random()*1000;
	var entrega = Math.random()*15 + 1;
	var detalles = "Detalles";

	$.ajax({
		url: "process.php?action=anadir_presupuesto",
		type: "POST",
		data: {total:precio, tiempoEntrega:entrega, detalle:detalles, repid:rid},
		success: function(data){
			cargar('reqaprobados');
		}
	});
}

function aceptarPresupuesto(repid, preid, divid)
{
	$.ajax({
		url: "process.php?action=aceptar_presupuesto",
		type: "POST",
		data: {rid:repid, pid:preid},
		success: function(data){
			$("#notif"+divid).html("Se ha añadido el recargo a la factura de este mes y se ha enviado una notificación a los inquilinos.");
			$("#notif"+divid).css("background-color", "rgb(98, 110, 8)");
		}
	});
}

function cargar(seccion)
{
	var archivo = "";

	if (seccion == "reunion")
		archivo = "php/reunion.php";
	else if (seccion == "requerimiento")
		archivo = "php/requerimiento.php";
	else if (seccion=="asamblea")
		archivo = "php/asamblea.php";
	else if (seccion=="perfil")
		archivo = "php/perfil.php";
	else if (seccion=="notificacion")
		archivo = "php/notificaciones.php";
	else if (seccion=="servicio")
		archivo = "php/servicio.php";
	else if (seccion=="presupuesto")
		archivo = "php/presupuesto.php";
	else if (seccion=="aceptarpresupuesto")
		archivo = "php/aceptarpresupuesto.php";
	else if (seccion == "buzon")
		archivo = "php/verbuzon.php";
	else if (seccion == "verfactura")
		archivo = "php/verfactura.php";
	else if (seccion == "reqaprobados")
		archivo = "php/reqaprobados.php";
	else if (seccion == "fondos")
		archivo = "php/fondos.php";
	else if (seccion == "addfond")
		archivo = "php/addfond.php";
	
	$.ajax({
		beforeSend: function(){
			/*main.html("<div class='loader'><img src='img/loader.gif'><br/>Loading...</div>");*/
			/* Este codigo corre ANTES de que llegue la respuesta */
		},
		url: archivo,
		type: "POST",
		data: {}, // Este va vacio siempre
		success: function(data){
			$("#respuesta").html(data);			
		}
	});
}