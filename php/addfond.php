<div class="wrapper wrapper-640" style="margin-top:-20px;">
		<form class="j-forms" id="j-forms" method="post" action="process.php?action=anadir_fondo">

			<div class="header">
				<p>Añadir fondo</p>
			</div>
			<!-- end /.header-->

			<div class="content">

				<div class="unit">
					<label class="label">Saldo total a añadir</label>
					<div class="input">
						<input type="text"  name="monto" required>
					</div>
				</div>
				<!-- start response from server -->
				<div id="response"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn">Añadir</button>
			</div>
			<!-- end /.footer -->

		</form>
	</div>