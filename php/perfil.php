<?php
	@session_start(); 
	include_once("databasehandler.php");
	$dbHandler = new DatabaseHandler();

	$u = $dbHandler->obtenerUsuario($_SESSION['username']);
?>

<div class="wrapper wrapper-640">
		<!-- este action seria de actualizar method="post" action="process.php?action=crear_usuario -->
		<form class="j-forms" id="j-forms">
			<div class="header">
				<p><?php echo $u['nombreCompleto']; ?></p>
			</div>
			<!-- end /.header -->

			<div class="content">

				<!-- start name -->
				<div class="j-row">
					<div class="span2">
						<label class="label label-center">Nombre</label>
					</div>
					<div class="span4 unit">
						<label class="label label-center"><?php echo $u['nombre']; ?></label>
					</div>
					<!-- aqui vendria un if de php si tiene o no segundo nombre para mostrarlo -->
					<div class="span2">
						<label class="label label-center"><?php echo ($u['segundoNombre'] != null) ? "2do. nombre" : "" ?></label>
					</div>
					<div class="span4 unit">
						<label class="label label-center"><?php echo $u['segundoNombre']; ?></label>
					</div>
					<!-- que termina aqui -->
				</div>
				<div class="j-row">
					<div class="span2">
						<label class="label label-center">Apellido*</label>
					</div>
					<div class="span4 unit">
						<label class="label label-center"><?php echo $u['apellido']; ?></label>
					</div>
					<!-- otro if si tiene segundo apellido o no -->
					<div class="span2">
						<label class="label label-center"><?php echo ($u['segundoApellido'] != null) ? "2do Apellido" : "" ?></label>
					</div>
					<div class="span4 unit">
						<label class="label label-center"><?php echo $u['segundoApellido']; ?></label>
					</div>
				</div>
				<!-- end name -->
				<div class="unit">
					<label class="label"><?php echo (count($u['tlfs']) > 0) ? 'Numeros de telefono registrados' : '<b>No tienes telefonos registrados</b>'; ?></label>
					<div>
						<?php foreach ($u['tlfs'] as $t): ?>
							<label class="label">
								<?php echo '('.$t['prefijo'].') - '.$t['num']; ?>
								<button type="button" class="btn btn-danger btn-xs">
									X
								</button>
							</label>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="j-row">
					<div class="span4">
						<label class="label label-center">Agregar num. Telefono</label>
					</div>
					<div class="span3 unit">
						<label class="input select">
							<select name="prefijo">
								<option value="0414">0414</option>
								<option value="0424">0424</option>
								<option value="0416">0416</option>
								<option value="0426">0426</option>
								<option value="0412">0412</option>
							</select>
							<i></i>
						</label>
					</div>
					<div class="span5 unit">
						<div class="input">
							<input type="text" id="Telefono" name="telefono" maxlength="7">
						</div>
					</div>
				</div>
				<!-- start email -->
				<div class="j-row">
					<div class="span4">
						<label class="label label-center">Actualizar correo</label>
					</div>
					<div class="span8 unit">
						<div class="input">
							<input type="email" id="email" name="email" placeholder="aqui correo si tiene" value="<?php echo $u['email']; ?>" />
						</div>
					</div>
				</div>
				<!-- end email -->

				<!-- start password -->
				<div class="j-row">
					<div class="span4">
						<label class="label label-center">Contraseña actual</label>
					</div>
					<div class="span8 unit">
						<div class="input">
							<input type="password" name="password" >
						</div>
					</div>
				</div>
				<div class="j-row">
					<div class="span4">
						<label class="label label-center">Contraseña nueva</label>
					</div>
					<div class="span8 unit">
						<div class="input">
							<input type="password" id="password" name="password" >
						</div>
					</div>
				</div>
				<div class="j-row">
					<div class="span4">
						<label class="label label-center">Confirmar nueva contraseña*</label>
					</div>
					<div class="span8 unit">
						<div class="input">
							<input type="password" id="confirm_password" name="confirm_password" placeholder="Ingrese nuevamente su clave">
						</div>
					</div>
				</div>
				<!-- end password -->
			</div>
			<!-- end /.content -->

			<div class="footer">
			<button type="submit" class="primary-btn" id="enable-button">Actualizar</button>
			</div>
			<!-- end /.footer -->

		</form>
</div>