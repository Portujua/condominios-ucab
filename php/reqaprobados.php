<?php 
	@session_start(); 
	include_once("databasehandler.php");
	$dbHandler = new DatabaseHandler();

	$reps = $dbHandler->obtenerReparaciones($_SESSION['username']);
?>
<div>
	<?php for ($i = 0; $i < count($reps); $i++): ?>
		<div class="notificacion" id="notif<?php echo $i; ?>">
			<strong><?php echo $reps[$i]['rep']['tipo']; ?></strong>
			<p>Detalles de la reparación</p>
			<p>Aprobado el: <?php echo $reps[$i]['rep']['fecha']; ?></p>
			<p><?php echo (count($reps[$i]['pre']) > 0) ? 'Presupuestos recibidos:' : 'No se tienen presupuestos aún.'; ?></p>
			<?php for ($k = 0; $k < count($reps[$i]['pre']); $k++): ?>
				<p>
					Presupuesto <?php echo ($k+1); ?>: <?php echo $reps[$i]['pre'][$k]['total']; ?> Bs.F
					Dias de entrega: <?php echo $reps[$i]['pre'][$k]['tiempo']; ?>
					<button type="button" class="btn btn-default" onclick="aceptarPresupuesto(<?php echo $reps[$i]['rep']['rid']; ?>, <?php echo $reps[$i]['pre'][$k]['pid']; ?>, <?php echo $i; ?>);">Aceptar presupuesto</button>
				</p>
			<?php endfor; ?>
			<button type="button" class="btn btn-default" onclick="generarPresupuesto(<?php echo $reps[$i]['rep']['rid']; ?>, <?php echo $i; ?>);">Pedir presupuesto</button>
		</div>
	<?php endfor; ?>
	<!--<div class="notificacion">
	<strong>Requerimiento 1</strong>
	<p>Detalles del requerimineto</p>
	<p>Aprobado el: 12-12-12</p>
	<p>Presupuestos recibidos:</p>
	<p>Presupuesto 1: 12341bs. Dias de entrega: 12 <button type="button" class="btn btn-default">Aceptar presupuesto</button></p>
	<p>Presupuesto 2: 12341bs. Dias de entrega: 12 <button type="button" class="btn btn-default">Aceptar presupuesto</button></p>
	<button type="button" class="btn btn-default">Pedir presupuesto</button>
	</div>-->
</div>