﻿<?php
	class DatabaseHandler
	{
		// Local
		private $username = "root";
		private $password = "21115476";
		private $dsn = "mysql:dbname=condominiosucab;host=localhost";

		// Marcos
		/*private $username = "salazars_folks";
		private $password = "21115476";
	    private $dsn = "mysql:dbname=salazars_folksandart;host=localhost";*/

		private $db;

		public function connect()
        {
            if (!$this->db instanceof PDO)
            {
                $this->db = new PDO($this->dsn, $this->username, $this->password);       
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }

            $this->db->query("SET CHARSET utf8");
        }

        public function dbDeleteAll()
        {
            $fileName = "../bd/script-drops.sql";
            $fp = fopen($fileName, "r");
            $content = fread($fp, filesize($fileName));
            fclose($fp);
            

            $query = $content;

            $this->connect();
            $this->db->query($query);
        }

        public function dbCreateAll()
        {
            $fileName = "../bd/script-creacion.sql";
            $fp = fopen($fileName, "r");
            $content = fread($fp, filesize($fileName));
            fclose($fp);
            

            $query = $content;

            $this->connect();
            $this->db->query($query);
        }

        public function dbAddBasicData()
        {
            $fileName = "../bd/script-datosiniciales.sql";
            $fp = fopen($fileName, "r");
            $content = fread($fp, filesize($fileName));
            fclose($fp);


            $query = $content;

            $this->connect();
            $this->db->query($query);
        }








        public function fixTimestamp($ts)
        {
            $ano = substr($ts, 0, 4);
            $mes = substr($ts, 5, 2);
            $dia = substr($ts, 8, 2);

            $hora = substr($ts, 11, 2);
            $min = substr($ts, 14, 2);
            $m = "a.m.";

            if ($hora > 12)
            {
                $hora -= 12;
                $m = "p.m.";
            }

            $full = $dia."/".$mes."/".$ano." a las ".$hora.":".$min." ".$m;

            return $full;
        }

        public function fixAntiSqlInject($text)
        {
            $text = str_replace("\"", "", $text);
            $text = str_replace("'", "", $text);
            return $text;
        }

        private $KEY = "oSsZoInSsCoToO";
    
        /**
         * Encriptar un string 
         * 
         * @param string $string El string que queremos encriptar
         * 
         * @return String
         */
        function encrypt($string) 
        {
            $iv = md5(md5($this->KEY));
            
            $enc = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->KEY), $string, MCRYPT_MODE_CBC, $iv);
            $enc = base64_encode($enc);
            
            return $enc;
        }

        /**
         * Encriptar un string 
         * 
         * @param string $string El string que queremos encriptar
         * 
         * @return String
         */
        function encryptForDb($string) 
        {
            $iv = md5(md5($this->KEY));
            
            $enc = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->KEY), $string, MCRYPT_MODE_CBC, $iv);
            $enc = base64_encode($enc);

            $enc = $this->fixAntiSqlInject($enc);
            
            return $enc;
        }
        
        /**
         * Desencripta un string
         * 
         * @param string $string El string que vamos a desencriptar
         */
        function decrypt($string) 
        {
            $iv = md5(md5($this->KEY));
            
            $dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->KEY), base64_decode($string), MCRYPT_MODE_CBC, $iv);
            $dec = rtrim($dec, "");
            
            return $dec;
        }

        public function getTodayDate()
		{
			date_default_timezone_set('America/Caracas');
			$today = array(
				"ano" => (int)date('Y', time()),
				"mes" => (int)date('m', time()),
				"dia" => (int)date('d', time()),
			);
			
			return $today;
		}

        public function getTodayDateString()
        {
            date_default_timezone_set('America/Caracas');
            $today = array(
                "ano" => (int)date('Y', time()),
                "mes" => (int)date('m', time()),
                "dia" => (int)date('d', time()),
            );
            
            return $today['dia'] . '-' . $today['mes'] . '-' . $today['ano'];
        }

        public function getTodayDateStringDB()
        {
            date_default_timezone_set('America/Caracas');
            $today = array(
                "ano" => (int)date('Y', time()),
                "mes" => (int)date('m', time()),
                "dia" => (int)date('d', time()),
            );
            
            return $today['ano'] . '-' . $today['mes'] . '-' . $today['dia'];
        }





        public function login($username, $password)
        {
            $query = "
                SELECT USUA_ID as cedula, USUA_CLAVE as clave, concat(USUA_NOMBRE, ' ', USUA_APELLIDO) as nombreCompleto, USUA_TIPO as tipo
                FROM Usuario
                WHERE USUA_ID='".$username."' AND USUA_CLAVE='".$password."'
            ";

            $this->connect();
            $ret = $this->db->query($query);
            $ret = $ret->fetchAll();

            $login = array();

            if (count($ret) > 0)
            {
                $login['nombre'] = $ret[0]['nombreCompleto'];
                $login['tipo'] = $ret[0]['tipo'];
            }
            else
                $login['tipo'] = -1;

            return $login;
        }



        public function obtenerUsuario($id)
        {
            $query = "
                SELECT USUA_NOMBRE as nombre, USUA_SEGUNDO_NOMBRE as segundoNombre, USUA_SEGUNDO_APELLIDO as segundoApellido, USUA_CORREO as email, USUA_APELLIDO as apellido, concat(USUA_NOMBRE, ' ', USUA_APELLIDO) as nombreCompleto
                FROM Usuario
                WHERE USUA_ID='".$id."'
            ";

            $this->connect();
            $ret = $this->db->query($query);
            $ret = $ret->fetchAll();

            $query = "
                SELECT TELEF_PREFIJO as prefijo, TELEF_NUMERO as num
                FROM Telefono
                WHERE FK_USUARIO='".$id."'
            ";

            $tlfs = $this->db->query($query);
            $ret[0]['tlfs'] = array();

            foreach ($tlfs as $t)
                $ret[0]['tlfs'][] = $t;

            return $ret[0];
        }

        public function obtenerJuntaIdPorUsuario($uid)
        {
            $query = "
                select JUNT_ID
                FROM Apartamento INNER JOIN Usua_Apar INNER JOIN Edificio INNER JOIN Junta_Condominio
                ON FK_APARTAMENTO=APAR_ID AND FK_EDIFICIO=EDIFICIO_ID AND FK_JCOND=JUNT_ID
                WHERE FK_USUARIO='".$uid."'
            ";

            $this->connect();
            $ret = $this->db->query($query);
            $ret = $ret->fetchAll();

            return $ret[0]['JUNT_ID'];
        }

        public function obtenerNotificacionesDeJunta($uid)
        {
            $query = "
                SELECT NOTIFICACION_TIPO as tipo, NOTIFICACION_DETALLES as detalles
                FROM (select JUNT_ID
                    FROM Apartamento INNER JOIN Usua_Apar INNER JOIN Edificio INNER JOIN Junta_Condominio
                    ON FK_APARTAMENTO=APAR_ID AND FK_EDIFICIO=EDIFICIO_ID AND FK_JCOND=JUNT_ID
                    WHERE FK_USUARIO='".$uid."') R RIGHT JOIN Notificacion
                ON FK_JCOND=JUNT_ID
                WHERE NOTIFICACION_TIPO='Informativo' AND FK_JCOND=JUNT_ID
                ORDER BY NOTIFICACION_ID DESC
            ";

            $this->connect();
            return $this->db->query($query);
        }

        public function obtenerNotificacionesDeAsamblea($uid)
        {
            $query = "
                SELECT RR.*, ASAM_FECHA_INICIO as fechaInicio
                FROM (SELECT NOTIFICACION_TIPO as tipo, NOTIFICACION_DETALLES as detalles, FK_ASAMBLEA
                    FROM (select JUNT_ID
                        FROM Apartamento INNER JOIN Usua_Apar INNER JOIN Edificio INNER JOIN Junta_Condominio
                        ON FK_APARTAMENTO=APAR_ID AND FK_EDIFICIO=EDIFICIO_ID AND FK_JCOND=JUNT_ID
                        WHERE FK_USUARIO='".$uid."') R RIGHT JOIN Notificacion
                    ON FK_JCOND=JUNT_ID
                    WHERE NOTIFICACION_TIPO='Asamblea' AND FK_JCOND=JUNT_ID) RR LEFT JOIN Asamblea
                ON FK_ASAMBLEA=ASAM_ID
                ORDER BY ASAM_FECHA_INICIO DESC
            ";

            $this->connect();
            return $this->db->query($query);
        }

        public function obtenerNotificacionesDeRequerimientos($uid)
        {
            $query = "
                SELECT RR.*, REQ_TEMA as tema, REQ_PRIORIDAD as prioridad
                FROM (SELECT NOTIFICACION_TIPO as tipo, NOTIFICACION_DETALLES as detalles, FK_REQUERIMIENTO
                    FROM (select JUNT_ID
                        FROM Apartamento INNER JOIN Usua_Apar INNER JOIN Edificio INNER JOIN Junta_Condominio
                        ON FK_APARTAMENTO=APAR_ID AND FK_EDIFICIO=EDIFICIO_ID AND FK_JCOND=JUNT_ID
                        WHERE FK_USUARIO='".$uid."') R RIGHT JOIN Notificacion
                    ON FK_JCOND=JUNT_ID
                    WHERE NOTIFICACION_TIPO='Requerimiento' AND FK_JCOND=JUNT_ID) RR LEFT JOIN Requerimiento
                ON FK_REQUERIMIENTO=REQ_ID
                ORDER BY REQ_ID DESC
            ";

            $this->connect();
            return $this->db->query($query);
        }

        public function obtenerBuzon($uid)
        {
            $jid = $this->obtenerJuntaIdPorUsuario($uid);

            $query = "
                SELECT R.*
                FROM (SELECT BUZON_FECHA as fecha, BUZON_TITULO as titulo, BUZON_SUGERENCIA as sugerencia, FK_USUARIO as uid, concat(USUA_NOMBRE, ' ', USUA_APELLIDO) as nombre, BUZON_ID as id
                    FROM Buzon LEFT JOIN Usuario
                    ON FK_USUARIO=USUA_ID) R INNER JOIN Usua_Apar INNER JOIN Apartamento INNER JOIN Edificio
                ON uid=FK_USUARIO AND FK_APARTAMENTO=APAR_ID AND FK_EDIFICIO=EDIFICIO_ID
                WHERE FK_JCOND=".$jid."
                ORDER BY fecha DESC
            ";

            $this->connect();
            return $this->db->query($query);
        }

        public function obtenerUltimaAsamblea()
        {
            $query = "
                SELECT ASAM_ID
                FROM Asamblea
                ORDER BY ASAM_ID DESC
            ";

            $this->connect();
            $ret = $this->db->query($query);
            $ret = $ret->fetchAll();

            return $ret[0]['ASAM_ID'];
        }

        public function obtenerInfoBuzon($bid)
        {
            $query = "
                SELECT BUZON_TITULO as titulo, BUZON_FECHA as fecha
                FROM Buzon
                WHERE BUZON_ID=".$bid."
            ";

            $this->connect();
            $ret = $this->db->query($query);
            $ret = $ret->fetchAll();

            return $ret[0];
        }       

        public function obtenerUltimaReparacion()
        {
            $query = "
                SELECT REPA_ID
                FROM Reparacion
                ORDER BY REPA_ID DESC
            ";

            $this->connect();
            $ret = $this->db->query($query);
            $ret = $ret->fetchAll();

            return $ret[0]['REQ_ID'];

        }

        public function obtenerAreasDeEdificio($uid)
        {
            $jid = $this->obtenerJuntaIdPorUsuario($uid);

            $query = "
                SELECT AREA_NOMBRE as nombre, AREA_ID as id
                FROM Edificio RIGHT JOIN Area
                ON FK_EDIFICIO=EDIFICIO_ID
                WHERE FK_JCOND=".$jid."
            ";

            $this->connect();
            return $this->db->query($query);
        }

        public function obtenerReparaciones($uid)
        {
            $jid = $this->obtenerJuntaIdPorUsuario($uid);
            $ret = array();

            $query = "
                SELECT REPA_ID as rid, REPA_TIPO as tipo, REPA_CULMINADA as culminada, REPA_FECHA_INICIO as fecha
                FROM (SELECT AREA_ID as aid
                    FROM Edificio RIGHT JOIN Area
                    ON FK_EDIFICIO=EDIFICIO_ID
                    WHERE FK_JCOND=".$jid.") R RIGHT JOIN Reparacion
                ON aid=FK_AREA
                WHERE REPA_CULMINADA=0
            ";

            $this->connect();
            $reparaciones = $this->db->query($query);

            foreach ($reparaciones as $r)
            {
                $p = array();

                $query = "
                    SELECT PRES_PRECIO_TOTAL as total, PRES_TIEMPO_ENTREGA as tiempo, PRES_DETALLE as detalles, PRES_ID as pid
                    FROM Presupuesto
                    WHERE FK_REPARACION=".$r['rid']."
                ";

                $presupuestos = $this->db->query($query);

                foreach ($presupuestos as $pr)
                    $p[] = $pr;

                $ret[] = array("rep" => $r, "pre" => $p);
            }

            return $ret;
        }

        public function obtenerFactura($uid)
        {
            $jid = $this->obtenerJuntaIdPorUsuario($uid);

            $query = "
                select PRES_PRECIO_TOTAL as total, REPA_TIPO as tipo, REPA_FECHA_INICIO as fecha
                from (select *
                    from (select area_id as aid
                        from Edificio RIGHT JOIN Area
                        on FK_EDIFICIO=EDIFICIO_ID 
                        where FK_JCOND=".$jid.") R right join Reparacion
                    on FK_AREA=aid
                    where REPA_FECHA_CULMINADA is not null) RR inner join Presupuesto
                on REPA_CULMINADA=PRES_ID
            ";

            $this->connect();
            return $this->db->query($query);
        }

        public function obtenerFondos($uid)
        {
            $jid = $this->obtenerJuntaIdPorUsuario($uid);

            $query = "
                SELECT FOND_SALDO_TOTAL as saldoTotal, FOND_SALDO_DISPONIBLE as saldoDisponible, FOND_INGRESO as ingresos, FOND_EGRESO as egresos
                FROM Fondo 
                WHERE FK_JCOND=".$jid."
            ";

            $this->connect();
            $ret = $this->db->query($query);
            $ret = $ret->fetchAll();

            return $ret[0];
        }

        public function obtenerMontoPresupuesto($pid)
        {
            $query = "
                SELECT PRES_PRECIO_TOTAL as monto
                FROM Presupuesto
                WHERE PRES_ID=".$pid."
            ";

            $this->connect();
            $ret = $this->db->query($query);
            $ret = $ret->fetchAll();

            return $ret[0]['monto'];
        }













        public function aceptarPresupuesto($data)
        {
            // Chequeo si hay saldo
            $jid = $this->obtenerJuntaIdPorUsuario($_SESSION['username']);
            $fondos = $this->obtenerFondos($_SESSION['username']);
            $costo = $this->obtenerMontoPresupuesto($data['pid']);

            if ($fondos['saldoDisponible'] < $costo)
                return false;




            // Lo hago
            $query = "
                UPDATE Reparacion SET REPA_CULMINADA=".$data['pid'].", REPA_FECHA_CULMINADA=DATE_ADD('".$this->getTodayDateStringDB()."', interval (select PRES_TIEMPO_ENTREGA from Presupuesto where pres_id=".$data['pid'].") day)
                WHERE REPA_ID=".$data['rid']."
            ";

            $this->connect();
            $this->db->query($query);

            $query = "
                UPDATE Fondo SET FOND_SALDO_DISPONIBLE=(".$fondos['saldoDisponible']."-".$costo."), FOND_SALDO_TOTAL=(".$fondos['saldoTotal']."-".$costo."), FOND_EGRESO=(".$fondos['egresos']."+".$costo.")
                WHERE FK_JCOND=".$jid."
            ";

            $this->db->query($query);

            // Genero la notificacion            

            $query = "
                INSERT INTO Notificacion (NOTIFICACION_TIPO, NOTIFICACION_DETALLES, FK_JCOND)
                VALUES ('Informativo', 'Se ha hecho una reparación, por lo que se ha añadido a la factura un 10% del costo de la reparación. Para más información acceda al menú de Ver Factura', ".$jid.")
            ";

            $this->db->query($query);
        }

        public function crearUsuario($data)
        {
            $query = "
                INSERT INTO Usuario (USUA_NOMBRE, USUA_APELLIDO, USUA_ID, USUA_CLAVE ".(($data['email'] != null) ? ', USUA_CORREO' : '').(($data['segundo-nombre'] != null) ? ', USUA_SEGUNDO_NOMBRE' : '').(($data['segundo-apellido'] != null) ? ', USUA_SEGUNDO_APELLIDO' : '').")
                VALUES ('".$data['nombre']."', '".$data['apellido']."', '".$data['cedula']."', '".$data['clave']."'".(($data['email'] != null) ? ", '".$data['email']."'" : '').(($data['segundo-nombre'] != null) ? ", '".$data['segundo-nombre']."'" : '').(($data['segundo-apellido'] != null) ? ", '".$data['segundo-apellido']."'" : '').")
            ";

            $this->connect();

            try {                
                $this->db->query($query);

                if ($data['telefono'] != null)
                {
                    $query = "
                        INSERT INTO Telefono (TELEF_PREFIJO, TELEF_NUMERO, FK_USUARIO)
                        VALUES ('".$data['prefijo']."', '".$data['telefono']."', '".$data['cedula']."')
                    ";

                    $this->db->query($query);
                }

                $_SESSION['username'] = $data['cedula'];
                $_SESSION['nombre'] = $data['nombre'] . " " . $data['apellido'];
                $_SESSION['tipo'] = 1;

                return true;
            }
            catch (Exception $e)
            {
                return false;
            }
        }

        public function crearAsamblea($data)
        {
            $jid = $this->obtenerJuntaIdPorUsuario($data['uid']);

            $query = "
                INSERT INTO Asamblea (ASAM_FECHA_INICIO, ASAM_TIPO, FK_JCOND)
                VALUES ('".$data['fecha']."', '".$data['tipo']."', ".$jid.")
            ";

            $this->connect();
            $this->db->query($query);

            // Generamos la notificacion
            $aid = $this->obtenerUltimaAsamblea();

            $query = "
                INSERT INTO Notificacion (NOTIFICACION_TIPO, NOTIFICACION_DETALLES, FK_ASAMBLEA, FK_JCOND)
                VALUES ('Asamblea', 'Se ha convocado a una asamblea el día ".$data['fecha']." de tipo ".$data['tipo'].", se agradece su asistencia.', ".$aid.", ".$jid.")
            ";

            $this->db->query($query);
        }

        public function anadirAlBuzon($data)
        {
            $this->connect();

            $query = "
                INSERT INTO Buzon (BUZON_FECHA, BUZON_TITULO, BUZON_SUGERENCIA, FK_USUARIO)
                VALUES ('".$this->getTodayDateStringDB()."', '".$data['tema']."', '".$data['msj']."', '".$data['uid']."')
            ";

            $this->db->query($query);
        }

        public function aprobarBuzon($bid, $area)
        {
            $this->connect();
            $jid = $this->obtenerJuntaIdPorUsuario($_SESSION['username']);

            $b = $this->obtenerInfoBuzon($bid);

            /* Añadimos el requerimiento */
            $query = "
                INSERT INTO Reparacion (REPA_FECHA_INICIO, REPA_TIPO, FK_AREA)
                VALUES ('".$b['fecha']."', '".$b['titulo']."', ".$area.")
            ";

            $this->db->query($query);

            /* Añadimos la notificacion */
            $query = "
                INSERT INTO Notificacion (NOTIFICACION_TIPO, NOTIFICACION_DETALLES, FK_JCOND)
                VALUES ('Informativo', 'La Junta de Condominio ha decidido hacer una reparación', ".$jid.")
            ";

            $this->db->query($query);

            /* Borramos el buzon */
            $this->eliminarBuzon($bid);
        }

        public function anadirPresupuesto($data)
        {
            $query = "
                INSERT INTO Presupuesto (PRES_PRECIO_TOTAL, PRES_TIEMPO_ENTREGA, PRES_DETALLE, FK_REPARACION)
                VALUES (".$data['total'].", ".$data['tiempo'].", '".$data['detalles']."', ".$data['rid'].")
            ";

            $this->connect();
            $this->db->query($query);
        }

        public function anadirFondo($monto, $uid)
        {
            $jid = $this->obtenerJuntaIdPorUsuario($uid);
            $fondos = $this->obtenerFondos($uid);

            $query = "
                UPDATE Fondo SET FOND_SALDO_DISPONIBLE=(".$fondos['saldoDisponible']."+".$monto."), FOND_SALDO_TOTAL=(".$fondos['saldoTotal']."+".$monto."), FOND_INGRESO=(".$fondos['ingresos']."+".$monto.")
                WHERE FK_JCOND=".$jid."
            ";

            $this->connect();
            $this->db->query($query);
        }







        public function eliminarBuzon($bid)
        {
            $query = "
                DELETE FROM Buzon WHERE BUZON_ID=".$bid."
            ";

            $this->connect();

            try {
                $this->db->query($query);
                return true;
            }
            catch (Exception $e)
            {
                return false;
            }
        }
	}
?>