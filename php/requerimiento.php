<div class="wrapper wrapper-640" style="margin-top:-20px;">
		<form class="j-forms" id="j-forms" method="post" action="process.php?action=crear_buzon">

			<div class="header">
				<p>Realizar sugerencia</p>
			</div>
			<!-- end /.header-->

			<div class="content">

				<!-- start name -->
				<div class="unit">
					<label class="label">Tema de la sugerencia</label>
					<div class="input">
						<input type="text"  name="reason" required>
					</div>
				</div>
				<!-- end name -->

				<!-- start message este iria en noti_detalles-->
				<div class="unit">
					<label class="label">Mas detalles de la sugerencia</label>
					<div class="input">
						<textarea spellcheck="false" name="message"></textarea>
					</div>
				</div>
				<!-- end message -->

				<!-- start response from server -->
				<div id="response"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn">Enviar</button>
			</div>
			<!-- end /.footer -->

		</form>
	</div>