﻿<?php

	if (!isset($_GET['action']))
	{
		echo "Selecciona una accion:<br>
				<a href='dbmanager.php?action=create'>Crear tablas</a><br>
				<a href='dbmanager.php?action=delete'>Eliminar tablas</a><br>
				<a href='dbmanager.php?action=add-basic'>Añadir datos iniciales</a>
				<br><br><br>
				<a href='../'>Volver a la página principal</a>";

		die();
	}


	include_once("databasehandler.php");
    $dbHandler = new DatabaseHandler();

	if ($_GET['action'] == "create")
	{
		$dbHandler->dbCreateAll();
		echo "Tablas creadas.<br><a href='dbmanager.php'>Volver</a>";
		die();
	}
	elseif ($_GET['action'] == "delete")
	{
		$dbHandler->dbDeleteAll();
		echo "Tablas eliminadas.<br><a href='dbmanager.php'>Volver</a>";
		die();
	}
	elseif ($_GET['action'] == "add-basic")
	{
		$dbHandler->dbAddBasicData();
		echo "Datos iniciales añadidos.<br><a href='dbmanager.php'>Volver</a>";
		die();
	}
?>