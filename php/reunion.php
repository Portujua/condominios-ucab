<div class="wrapper wrapper-640" style="margin-top:-20px;">
		<form class="j-forms" id="j-forms">

			<div class="header">
				<p>Convocar Reunion</p>
			</div>
			<!-- end /.header-->

			<div class="content">

				<!-- start name -->
				<div class="unit">
					<label class="label">Motivo de la reunion</label>
					<div class="input">
						<input type="text"  name="reason" required>
					</div>
				</div>
				<!-- end name -->
				<!-- date of meeting -->

				<div class="unit">
					<label class="label">Fecha de la reunión</label>
					<div class="input">
						<input type="date" name="Mday" required>
					</div>


				</div>
				<div class="unit">
					<label class="label">Hora inicio</label>
					<div class="input">
						<input type="time" name="Shour" required>
					</div>
				</div>		
				<!-- end date of meeting -->

				<div class="divider gap-bottom-25"></div>

				<!-- start message este iria en noti_detalles-->
				<div class="unit">
					<label class="label">Mas detalles de la reunion</label>
					<div class="input">
						<textarea spellcheck="false" name="message"></textarea>
					</div>
				</div>
				<!-- end message -->

				<!-- start response from server -->
				<div id="response"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn">Convocar!</button>
			</div>
			<!-- end /.footer -->

		</form>
	</div>