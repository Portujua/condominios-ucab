<div class="wrapper wrapper-640">
		<!-- aqui tiene que ir un if de los 3 presupuestos -->
		<form class="j-forms" id="j-forms">
			<div class="header">
				<p>Aceptar Presupuesto</p>
			</div>
			<!-- end /.header -->

			<div class="content">
					<div class="j-row">
						<div class="span4">
							<label class="label label-center">Escoja el presupuesto</label>
						</div>
						<div class="span8 unit">
							<label class="input select">
								<select name="prefijo">
									<option value="0">Primer presupuesto 12341 bs. 5 dias</option>
									<option value="1">Segundo presupuesto 4123 bs. 10 dias</option>
									<option value="2">Tercer presupuesto 14123 bs. 4 dias</option>
								</select>
								<i></i>
							</label>
						</div>
			</div>
			<!-- end /.content -->

			<div class="footer">
			<button type="submit" class="primary-btn" id="enable-button">Aceptar presupuesto</button>
			<label class="label label-center">*Al aceptar el presupuesto se cargará automaticamente a las facturas siguientes a cobrar</label>
			</div>
			<!-- end /.footer -->

		</form>
</div>