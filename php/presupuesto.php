<!-- este deberia ir en un boton que este en cada reparacion algo como "generar presupuesto" asi se referencia de que reparacion es -->
<div class="wrapper wrapper-640" style="margin-top:-20px;">
		<form class="j-forms" id="j-forms">

			<div class="header">
				<p>Generar presupuesto</p>
			</div>
			<!-- end /.header-->

			<div class="content">

				<!-- start name -->
				<div class="unit">
					<label class="label">Tipo del servicio</label>
					<div class="input">
						<input type="text"  name="type" required>
					</div>
				</div>
				<!-- end name -->
				<!-- start priority -->
				<div class="unit">
					<label class="label">Costo total</label>
					<div class="input">
						<input type="text"  name="price" required>
					</div>
				</div>
				<!-- end priority -->
				<div class="j-row">
					<div class="span4">
						<label class="label label-center">Tiempo de entrega</label>
					</div>
					<div class="span5 unit">
						<div class="input">
							<input type="text" id="tiempo" name="tiempo" placeholder="Expresado en dias">
						</div>
					</div>
				</div>

				<div class="divider gap-bottom-25"></div>

				<!-- start message este iria en noti_detalles-->
				<div class="unit">
					<label class="label">Mas detalles del presupuesto</label>
					<div class="input">
						<textarea spellcheck="false" name="message"></textarea>
					</div>
				</div>
				<!-- end message -->

				<!-- start response from server -->
				<div id="response"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn">Enviar</button>
			</div>
			<!-- end /.footer -->

		</form>
	</div>