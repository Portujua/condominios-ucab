<?php
	@session_start(); 
	include_once("databasehandler.php");
	$dbHandler = new DatabaseHandler();

	$repas = $dbHandler->obtenerFactura($_SESSION['username']);
?>

<div class="container-fluid">
	<div class="col-md-8">
		<div class="row">
			<img class="img-responsive logo" src="img/logo.png" alt="Condominios UCAB"/>
		</div>
		<div class="row notificacion">
			<h2>NOMBRE DE LA RESIDENCIA</h2>

			<h2>NOMBRE DE LOS DUEÑOS</h2>
		</div>
		<div class="row">

			<table  width="100%" border="1">
			  <tr>
			    <th scope="col" >Concepto de gasto</th>
			    <th scope="col" >Fecha de facturacion</th>
			    <th scope="col" >Monto facturado</th>
			    <th scope="col" >Monto por su parte</th>
			  </tr>
			 
			  <tr>
			    <td >Servicios básicos</td>
			    <td >06-2015</td>
			    <td id="montoGrande">200 Bs.F</td>
			    <td id="monto">200.00 bs.</td>
			  </tr>

			  <?php foreach ($repas as $r): ?>
			  	<tr>
				    <td ><?php echo $r['tipo']; ?></td>
				    <td ><?php echo $r['fecha']; ?></td>
				    <td id="montoGrande"><?php echo $r['total']; ?> Bs.F</td>
				    <td id="monto"><?php echo ($r['total']/10); ?> Bs.F</td>
				  </tr>
			  <?php endforeach; ?>

			  <tr>
			    <th >&nbsp;</th>
			    <td >&nbsp;</td>
			    <td >&nbsp;</td>
			    <td ><strong>&nbsp;</strong></td>
			  </tr>

			  <tr>
			    <th scope="row" >SUBTOTAL</th>
			    <td >-</td>
			    <td >-</td>
			    <td id="subtotal"><strong></strong></td>
			  </tr>

			  <tr>
			    <th scope="row" >IVA (12%)</th>
			    <td >-</td>
			    <td >-</td>
			    <td id="iva"><strong></strong></td>
			  </tr>

			  <tr>
			    <th scope="row" >TOTAL</th>
			    <td >-</td>
			    <td >-</td>
			    <td id="total"><strong></strong></td>
			  </tr>
			</table>
			
		</div>
	</div>
	<div class="col-md-4">
		<table width="100%" border="1" style="text-align:center;">
			<tr>
				<td>
					<p>Fecha: <?php echo $dbHandler->getTodayDateString(); ?></p>
					<br>
					<P>AVISO DE COBRO</P>
				</td>
			</tr>
			<tr>
				<td>
					<p>Numero de recibo: 1</p>
					<br>
					<p>Numero de apto: 12</p>
				</td>
			</tr>
			<tr>
				<td>
					<p id="montoComun"></p>
					<br>
					<p></p>
					<p id="montoAPagar">Monto a pagar: 4000 bs.</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>De no cancelar el 30 de cada mes se generan cargos adicionales por mora en su siguiente factura del 1%</p>
				</td>
			</tr>
		</table>
	</div>
</div>

<script>
	function calcularFactura()
	{
		var subtotal = 0.0;
		var iva = 0.12;
		var montoGrande = 0.0;

		$("td#monto").each(function(){
			subtotal += parseFloat($(this).html());			
		});

		$("td#montoGrande").each(function(){
			montoGrande += parseFloat($(this).html());			
		});		

		subtotal = Math.round(subtotal*100)/100;
		var ivaCalc = Math.round(subtotal*iva*100)/100;
		var total = Math.round(subtotal*(iva+1)*100)/100;

		$("#subtotal strong").html(subtotal + " Bs.F");
		$("#iva strong").html(ivaCalc + " Bs.F");
		$("#total strong").html(total + " Bs.F");
		$("#montoComun").html("Monto común: " + Math.round((subtotal-200.00)*100)/100 + " Bs.F");
		$("#montoAPagar").html("Monto a pagar: " + Math.round((montoGrande)*100)/100 + " Bs.F");
	}

	calcularFactura();
</script>