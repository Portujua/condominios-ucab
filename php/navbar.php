<?php 
	if (isset($_POST))
	{
		$campoUsuario = "log";
		$campoClave = "password";
		$checkLogin = "logged";

		if (isset($_POST[$campoUsuario]))
		{
			$login = $dbHandler->login($_POST[$campoUsuario], $_POST[$campoClave]);
			if ($login['tipo'] != -1)
			{
				$_SESSION['username'] = $_POST[$campoUsuario];
				$_SESSION['nombre'] = $login['nombre'];
				$_SESSION['tipo'] = ($login['tipo'] == 0) ? "admin" : "otro";
				header("Location: ./");
			}
			else
				header("Location: ./?error=badlogin");
		}
	}

	if (isset($_GET['do']))
	{
		if ($_GET['do'] == "logout")
		{
			$_SESSION = array();
			header("Location: ./");
		}
	}
?>

<div class="container-fluid barranav" >
		
		<a href=""><img class="img-responsive logo" src="img/logo.png" alt="Condominios UCAB" /></a>	
		            <!-- Trigger the modal with a button -->
        <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="margin-top:90px;">Open Modal</button> -->
       	<?php if (!isset($_SESSION['username'])): ?> 
	       	<div class="container menu">
	        <!-- Button HTML (to Trigger Modal) -->
				<a href="#register-form" class="btn btn-large btn-primary modal-open" role="button">Registrarse</a>
				<a href="#login-form" class="btn btn-large btn-primary modal-open" role="button">Entrar</a>

			</div>
	    <?php else: ?>
	      	<div class="container menu">
	      		<p>Hola <?php echo $_SESSION['nombre']; ?></p>
				<a href="./?do=logout" class="btn btn-large btn-primary" role="button">Salir</a>

			</div>
		<?php endif; ?>
	<!-- registro -->
	<div class="modal-form" id="register-form">
			<div class="wrapper wrapper-640">

			<!-- <form action="j-folder/php/demo.php" method="post" class="j-forms" id="j-forms" novalidate> -->
			<form class="j-forms" id="j-forms" method="post" action="process.php?action=crear_usuario">
				<div class="header">
					<p>Registrarse</p>
					<!-- start close-modal button -->
					<label class="modal-close">
						<i></i>
					</label>
					<!-- end close-modal button -->
				</div>
				<!-- end /.header -->

				<div class="content">

					<!-- start name -->
					<div class="j-row">
						<div class="span2">
							<label class="label label-center">Nombre*</label>
						</div>
						<div class="span4 unit">
							<div class="input">
								<input type="text" id="name" name="name">
							</div>
						</div>
												<div class="span2">
							<label class="label label-center">2do. nombre</label>
						</div>
						<div class="span4 unit">
							<div class="input">
								<input type="text" name="name2" placeholder="opcional">
							</div>
						</div>
					</div>
					<div class="j-row">
						<div class="span2">
							<label class="label label-center">Apellido*</label>
						</div>
						<div class="span4 unit">
							<div class="input">
								<input type="text" id="lname" name="lname">
							</div>
						</div>
						<div class="span2">
							<label class="label label-center">2do Apellido</label>
						</div>
						<div class="span4 unit">
							<div class="input">
								<input type="text" name="name4" placeholder="opcional">
							</div>
						</div>
					</div>
					<!-- end name -->
					<div class="j-row">
						<div class="span4">
							<label class="label label-center">Num. Telefono</label>
						</div>
						<div class="span3 unit">
							<label class="input select">
								<select name="prefijo">
									<option value="0414">0414</option>
									<option value="0424">0424</option>
									<option value="0416">0416</option>
									<option value="0426">0426</option>
									<option value="0412">0412</option>
								</select>
								<i></i>
							</label>
						</div>
						<div class="span5 unit">
							<div class="input">
								<input type="text" id="Telefono" name="telefono" maxlength="7">
							</div>
						</div>
					</div>
					<!-- start email -->
					<div class="j-row">
						<div class="span4">
							<label class="label label-center">Correo*</label>
						</div>
						<div class="span8 unit">
							<div class="input">
								<input type="email" id="email" name="email">
							</div>
						</div>
					</div>
					<!-- end email -->

					<!-- start login -->
					<div class="j-row">
						<div class="span4">
							<label class="label label-center">CI*</label>
						</div>
						<div class="span8 unit">
							<div class="input">
								<input type="text" id="login" name="login" placeholder="Se usará para su ingreso">
							</div>
						</div>
					</div>
					<!-- end login -->

					<!-- start password -->
					<div class="j-row">
						<div class="span4">
							<label class="label label-center">Contraseña*</label>
						</div>
						<div class="span8 unit">
							<div class="input">
								<input type="password" id="password" name="password" >
							</div>
						</div>
					</div>
					<div class="j-row">
						<div class="span4">
							<label class="label label-center">Confirmar contraseña*</label>
						</div>
						<div class="span8 unit">
							<div class="input">
								<input type="password" id="confirm_password" name="confirm_password" placeholder="Ingrese nuevamente su clave">
							</div>
						</div>
					</div>
					<!-- end password -->

				<!-- start response from server -->
				<!--<div id="response"></div>-->
				<!-- end response from server -->

				</div>
				<!-- end /.content -->

				<div class="footer">
				<button type="submit" class="primary-btn" id="enable-button">Registrar</button>
				</div>
				<!-- end /.footer -->

			</form>
			</div>
	</div>
    <!-- login -->
    <div class="modal-form" id="login-form">
			<div class="wrapper wrapper-400">

			<!-- <form action="j-folder/php/demo.php" method="post" class="j-forms" id="j-forms" novalidate> -->
			<form class="j-forms" id="j-forms" method="post">
				<div class="header">
					<p>ENTRAR</p>
					<!-- start close-modal button -->
					<label class="modal-close">
						<i></i>
					</label>
					<!-- end close-modal button -->
				</div>
				<!-- end /.header -->

				<div class="content">

					<!-- start login -->
					<div class="j-row">
						<div class="span4">
							<label class="label label-center">CI</label>
						</div>
						<div class="span8 unit">
							<div class="input">
								<input type="text" id="log" name="log" required/>
							</div>
						</div>
					</div>
					<!-- end login -->

					<!-- start password -->
					<div class="j-row">
						<div class="span4">
							<label class="label label-center">Contraseña</label>
						</div>
						<div class="span8 unit">
							<div class="input">
								<input type="password" id="password" name="password" required="true"/>
							</div>
						</div>
					</div>
					<!-- end password -->

					<!-- start keep logged -->
					<div class="j-row">
						<div class="offset4 span8 unit">
							<label class="checkbox-toggle">
								<input type="checkbox" name="logged" value="true" checked="">
								<i></i>
								Seguir conectado
							</label>
						</div>
					</div>
					<!-- end keep logged -->

					<!-- start response from server -->
					<div id="response"></div>
					<!-- end response from server -->

				</div>
				<!-- end /.content -->

				<div class="footer">
					<button type="submit" class="primary-btn">Entrar</button>
				</div>
				<!-- end /.footer -->

			</form>
			</div>
    </div>	

</div>
