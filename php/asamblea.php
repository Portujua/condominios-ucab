<div class="wrapper wrapper-640" style="margin-top:-20px;">
		<form class="j-forms" id="j-forms" method="post" action="process.php?action=crear_asamblea">

			<div class="header">
				<p>Convocar asamblea</p>
			</div>
			<!-- end /.header-->

			<div class="content">

				<!-- start name -->
				<div class="unit">
					<label class="label">Tipo de asamblea</label>
					<div class="input">
						<input type="text"  name="reason" required>
					</div>
				</div>
				<!-- end name -->


				<div class="divider gap-bottom-25"></div>

				<div class="unit">
					<label class="label">Fecha de la asamblea</label>
					<div class="input">
						<input type="date" name="Mday" required>
					</div>
				</div>
				<div class="unit">
					<label class="label">Hora inicio</label>
					<div class="input">
						<input type="time" name="Shour" required>
					</div>
				</div>
				<!-- start response from server -->
				<div id="response"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn">Enviar</button>
			</div>
			<!-- end /.footer -->

		</form>
	</div>