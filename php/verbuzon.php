<?php
	@session_start(); 
	include_once("databasehandler.php");
	$dbHandler = new DatabaseHandler();

	$reqs = $dbHandler->obtenerBuzon($_SESSION['username']);
?>

<div id="ver_notificaciones" style="float:left;">
	<?php foreach ($reqs as $r): ?>
		<div class="notificacion" id="notif<?php echo $r['id']; ?>">
			<h4><?php echo $r['titulo'] . " (". $r['nombre'] . " el " . $r['fecha'] . "):"; ?></h4>
			<?php echo $r['sugerencia']; ?>
			<br/><br/>
			Aplicar en
			<select name"area" id="area<?php echo $r['id']; ?>">
				<?php $areas = $dbHandler->obtenerAreasDeEdificio($_SESSION['username']); ?>
				<?php foreach ($areas  as $a): ?>
					<option value="<?php echo $a['id']; ?>"><?php echo $a['nombre']; ?></option>
				<?php endforeach; ?>
			</select>
			<button class="btn btn-success" onclick="aprobar(<?php echo $r['id']; ?>);">Aprobar</button><button class="btn btn-danger" onclick="denegar(<?php echo $r['id']; ?>);">Denegar</button>
		</div>
	<?php endforeach; ?>
</div>