<div class="wrapper wrapper-640" style="margin-top:-20px;">
		<form class="j-forms" id="j-forms">

			<div class="header">
				<p>Solicitar servicio</p>
			</div>
			<!-- end /.header-->

			<div class="content">

				<!-- start name -->
				<div class="unit">
					<label class="label">Tipo del servicio</label>
					<div class="input">
						<input type="text"  name="type" required>
					</div>
				</div>
				<!-- end name -->
				<!-- start priority -->
				<div class="unit">
					<label class="label">Costo del servicio</label>
					<div class="input">
						<input type="text"  name="price" required>
					</div>
				</div>
				<!-- end priority -->

				<div class="divider gap-bottom-25"></div>

				<!-- start message este iria en noti_detalles-->
				<div class="unit">
					<label class="label">Mas detalles de la solicitud</label>
					<div class="input">
						<textarea spellcheck="false" name="message"></textarea>
					</div>
				</div>
				<!-- end message -->

				<!-- start response from server -->
				<div id="response"></div>
				<!-- end response from server -->

			</div>
			<!-- end /.content -->

			<div class="footer">
				<button type="submit" class="primary-btn">Enviar</button>
			</div>
			<!-- end /.footer -->

		</form>
	</div>