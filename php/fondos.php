<?php
	@session_start(); 
	include_once("databasehandler.php");
	$dbHandler = new DatabaseHandler();

	$fondo = $dbHandler->obtenerFondos($_SESSION['username']);
?>
<div>
	<div class="notificacion">
		<p>Saldo total del fondo: <?php echo $fondo['saldoTotal']; ?> Bs.F</p>
		<p>Saldo disponible: <?php echo $fondo['saldoDisponible']; ?> Bs.F</p>
		<p>Ingresos: <?php echo $fondo['ingresos']; ?> Bs.F</p>
		<p>Egresos: <?php echo $fondo['egresos']; ?> Bs.F</p>
	</div>
</div>