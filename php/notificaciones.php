<?php
	@session_start(); 
	include_once("databasehandler.php");
	$dbHandler = new DatabaseHandler();

	$notifJunta = $dbHandler->obtenerNotificacionesDeJunta($_SESSION['username']);
	$notifReq = $dbHandler->obtenerNotificacionesDeRequerimientos($_SESSION['username']);
	$notifAsamblea = $dbHandler->obtenerNotificacionesDeAsamblea($_SESSION['username']);
?>

<div id="ver_notificaciones" style="float:left;">
	<h3>Asambleas:</h3>
	<?php foreach ($notifAsamblea as $n): ?>
		<div class="notificacion">
			<b><?php echo $n['tipo']; ?> el <?php echo $dbHandler->fixTimestamp($n['fechaInicio']); ?></b>: <?php echo $n['detalles']; ?>
		</div>
	<?php endforeach; ?>

	<h3>Requerimientos:</h3>
	<?php foreach ($notifReq as $n): ?>
		<div class="notificacion">
			<b>Requerimiento:</b> <?php echo $n['tema']; ?><br/>
			<b>Prioridad:</b> <?php echo $n['prioridad']; ?><br/>
			<b>Detalles:</b> <?php echo $n['detalles']; ?>
		</div>
	<?php endforeach; ?>

	<h3>Otros:</h3>
	<?php foreach ($notifJunta as $n): ?>
		<div class="notificacion">
			<b><?php echo $n['tipo']; ?></b>: <?php echo $n['detalles']; ?>
		</div>
	<?php endforeach; ?>

	
	<!--<div class="notificacion">
		Detalles de la notificacion
	</div>
	<div class="notificacion">
		Detalles de la notificacion
	</div>
	<div class="notificacion">
		Detalles de la notificacion
	</div>-->
</div>